msgid ""
msgstr ""
"Project-Id-Version: Minetest textdomain rp_formspec x.x.x\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: \n"
"X-Generator: mtt_convert 0.1.0\n"

msgid "Fuzzy"
msgstr ""

msgid "Any fuzzy block"
msgstr ""

msgid "Planks"
msgstr ""

msgid "Any planks"
msgstr ""

msgid "Soil"
msgstr ""

msgid "Any soil"
msgstr ""

msgid "Stone"
msgstr ""

msgid "Any stone"
msgstr ""

msgid "Green Grass Clump"
msgstr ""

msgid "Any green grass clump"
msgstr ""

msgid "Paint Bucket"
msgstr ""

msgid "Any paint bucket"
msgstr ""

msgid "Non-full Paint Bucket"
msgstr ""

msgid "Any paint bucket that isn’t full"
msgstr ""

msgid "Group: @1"
msgstr ""

msgid "Write"
msgstr ""

